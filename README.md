# Training Devops #

[![Devops Overview](img/01-intro-devops.jpg)](img/01-intro-devops.jpg)

## Materi Training ##

Sesi 1
- Konsep Cloud Services
- Berbagai Jenis Cloud Services (IaaS, PaaS, BaaS, SaaS)
- Deployment Aplikasi ke IaaS
- Deployment Aplikasi ke PaaS

Sesi 2
- Konsep Dasar DevOps
- Infrastructure as Code
- Konfigurasi VPS dengan Puppet
- Konfigurasi VPS dengan Ansible
- Konsep Version Control
- Penggunaan Version Control dalam DevOps

Sesi 3
- Intro Docker
- Arsitektur Docker (Docker Machine, Docker Engine, Docker Host, Docker Swarm)
- Ekosistem Docker (Docker Image, Docker Registry)
- Workflow Development dengan Docker
- Membuat Dockerfile
- Instalasi Docker Machine
- Setup Docker Host
- Menjalankan Dockerfile
- Konfigurasi Docker Swarm

Sesi 4
- Konsep Kubernetes
- Arsitektur Kubernetes (Pods, Services, Volume, Networking)
- Berbagai penyedia Kubernetes hosting
- Berbagai distribusi Kubernetes
- Membuat konfigurasi pods
- Deklarasi persistence volume
- Menghubungkan pods, services, volume
- Deployment ke Kubernetes Hosting

Sesi 5
- Konsep Continuous Delivery
- Database Migration
- Automated Testing
- Setup Gitlab CI
- Setup Target Deployment
- Automated Build
- Automated Deployment

Sesi 6
- Release Management Workflow
- Deployment Environment (Dev, Test, Prod)
- Single Binary for Multiple Environment
- Canary Deployment
- Product Development & Release
- Konfigurasi Deployment Environment
- Database Refactoring Scenario
- Forward Only Migration

## Kebutuhan Software

* Git Client

    * Command Line
    * GUI : SourceTree

* Putty + Putty Gen
* Java SDK
* Apache Maven
* Netbeans 10
* MySQL Server (bisa pakai XAMPP)
* Docker

Ini tambahan di detached head